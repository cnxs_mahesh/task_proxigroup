$(document).ready(function(){
  $("#filter").select2({
      placeholder: "Select Areas",
      width: '100%'
  })

  var table=$("#records").DataTable({
            "bAutoWidth": true,
            stateSave: true,
            responsive: true,
            'info': false
          });

  $('#from_date').daterangepicker({
        startDate: moment(),
        autoUpdateInput: false,
    },
    function(start, end, label) {
        $('#from_date').val(start.format('YYYY-MM-DD'));
        $('#to_date').val(end.format('YYYY-MM-DD'));
    });

    $('#duration').timeDurationPicker({
        defaultValue: function() {
          return $('#seconds').val();
        },
        onSelect: function(element, seconds, duration) {
          $('#seconds').val(seconds);
          $('#duration').val(duration);
        }
      });

$(document).on('click', '.search', function(event) {
      var formObj = $("#formdata");
      var formData = new FormData(formObj[0]);
      $.ajax({
          url: 'search1.php',
          type: "POST",
          data:  formData,
          async: true,
          datatype : false,
          contentType: false,
          cache: false,
          processData:false,
          success: function(data, textStatus, jqXHR)
          {
            if(data=="")
            {
                table.clear().draw();
            }else{
                $("#targetBody").html(data);
            }
            
          }
      });
    })
})

var geocoder  = new google.maps.Geocoder();
var x = document.getElementById("demo");
function savePosition(position) {
  var add='';
    //var location  = new google.maps.LatLng(26.466853, 31.692535);    // turn coordinates into an object
    var location  = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);    // turn coordinates into an object
    geocoder.geocode({'latLng': location}, function (results, status) {
    if(status == google.maps.GeocoderStatus.OK) {  // if geocode success
        add=results[0].formatted_address;         // if address found, pass to processing function
        x.innerHTML = add;
        let devicePosition={
          'lat':position.coords.latitude,
          'lon':position.coords.longitude,
          'address': add,
          'accuracy': position.coords.accuracy,
          'alt' : position.coords.altitude,
          'altacc' :position.coords.altitude,
          'heading' : position.coords.heading,
          'speed' : position.coords.speed,
          'timestamp' : moment().format("YYYY-MM-DD HH:mm:ss"),
          'realtime' : position.timestamp
        };
        $.post("savedetails.php", devicePosition,
          function(data, textStatus, xhr) {
              if(xhr.status==200)
              {
                console.log(data);
              }
            });
      }
    });
}


function showError(error) {
    switch(error.code) {
        case error.PERMISSION_DENIED:
            x.innerHTML = "User denied the request for Geolocation."
            break;
        case error.POSITION_UNAVAILABLE:
            x.innerHTML = "Location information is unavailable."
            break;
        case error.TIMEOUT:
            x.innerHTML = "The request to get user location timed out."
            break;
        case error.UNKNOWN_ERROR:
            x.innerHTML = "An unknown error occurred."
            break;
    }
}

document.onreadystatechange = function () {
     if (document.readyState == "complete") {
       if (navigator.geolocation) {
         var gcp=navigator.geolocation.getCurrentPosition(savePosition,showError);
       } else {
           x.innerHTML = "Geolocation is not supported by this browser.";
       }
   }
 }
