<?php
require('class.database.php');
$db = Database::getInstance();
$mysqli = $db->getConnection();
//get the cookie verified or else
$ip_address=get_ip_address();
if (!isset($_COOKIE['g_id'])) {
  $uid = md5($_SERVER['HTTP_USER_AGENT'] .  $_SERVER['REMOTE_ADDR']);
  setcookie( "g_id", $uid, strtotime( '+1 year' ) );
}
if(isset($_COOKIE['g_id'])&& strlen(trim($_COOKIE['g_id']))>0)
{
  $sql_query = 'INSERT INTO tbl_user_activity (g_id,ip_address,lat,lon,address,accuracy,alt,altacc,heading,speed,timestamp,realtime)
  VALUES ("'.$_COOKIE['g_id'].'","'.$ip_address.'","'.$_POST['lat'].'","'.$_POST['lon'].'","'.$_POST['address'].'","'.$_POST['accuracy'].'",
  "'.$_POST['alt'].'","'.$_POST['altacc'].'","'.$_POST['heading'].'","'.$_POST['speed'].'","'.$_POST['timestamp'].'","'.$_POST['realtime'].'")';
  $result = $mysqli->query($sql_query);
}
//echo $uid;

function get_ip_address() {
  // check for shared internet/ISP IP
  if (!empty($_SERVER['HTTP_CLIENT_IP']) && validate_ip($_SERVER['HTTP_CLIENT_IP'])) {
      return $_SERVER['HTTP_CLIENT_IP'];
  }
  // check for IPs passing through proxies
  if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
      // check if multiple ips exist in var
      if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
          $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
          foreach ($iplist as $ip) {
              if (validate_ip($ip))
                  return $ip;
          }
      } else {
          if (validate_ip($_SERVER['HTTP_X_FORWARDED_FOR']))
              return $_SERVER['HTTP_X_FORWARDED_FOR'];
      }
  }
  if (!empty($_SERVER['HTTP_X_FORWARDED']) && validate_ip($_SERVER['HTTP_X_FORWARDED']))
      return $_SERVER['HTTP_X_FORWARDED'];
  if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && validate_ip($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
      return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
  if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && validate_ip($_SERVER['HTTP_FORWARDED_FOR']))
      return $_SERVER['HTTP_FORWARDED_FOR'];
  if (!empty($_SERVER['HTTP_FORWARDED']) && validate_ip($_SERVER['HTTP_FORWARDED']))
      return $_SERVER['HTTP_FORWARDED'];
  // return unreliable ip since all else failed
  return $_SERVER['REMOTE_ADDR'];
}
//echo get_ip_address();
?>
