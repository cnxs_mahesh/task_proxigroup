-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 29, 2018 at 03:00 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `proxigroup`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_activity`
--

CREATE TABLE `tbl_user_activity` (
  `id` int(11) NOT NULL,
  `g_id` varchar(50) NOT NULL,
  `ip_address` varchar(32) NOT NULL,
  `lat` decimal(10,8) NOT NULL,
  `lon` decimal(10,8) NOT NULL,
  `address` text NOT NULL,
  `accuracy` int(11) NOT NULL,
  `alt` varchar(50) DEFAULT NULL,
  `altacc` varchar(50) DEFAULT NULL,
  `heading` varchar(50) DEFAULT NULL,
  `speed` varchar(50) DEFAULT NULL,
  `timestamp` datetime NOT NULL,
  `realtime` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_activity`
--

INSERT INTO `tbl_user_activity` (`id`, `g_id`, `ip_address`, `lat`, `lon`, `address`, `accuracy`, `alt`, `altacc`, `heading`, `speed`, `timestamp`, `realtime`) VALUES
(1, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-02 21:43:45', '1522167225677'),
(2, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-02-13 21:49:22', '1522167562240'),
(3, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-07 21:49:46', '1522167586647'),
(4, 'b4c724c6d5149c71d14a121340b0996f', '::1', '17.91491600', '73.65075000', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-10 21:50:57', '1522167657482'),
(5, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-02-12 21:51:20', '1522167680418'),
(6, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-02-13 21:52:36', '1522167756089'),
(7, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-14 11:40:13', '1522217413312'),
(8, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-15 11:44:39', '1522217678919'),
(9, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-16 11:47:13', '1522217833443'),
(10, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-02-17 11:57:00', '1522218419751'),
(11, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-02-19 12:06:27', '1522218986754'),
(12, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-02-21 12:07:37', '1522219057582'),
(13, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-28 12:15:47', '1522219546717'),
(14, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-28 12:27:29', '1522220249382'),
(15, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-28 12:27:39', '1522220259383'),
(16, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-28 12:28:39', '1522220319073'),
(17, 'b4c724c6d5149c71d14a121340b0996c', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-28 12:28:52', '1522220332564'),
(18, 'b4c724c6d5149c71d14a121340b0996f', '::1', '19.07598400', '72.87765600', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-28 12:29:06', '1522220346172'),
(19, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-28 14:35:18', '1522227918416'),
(20, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-28 14:35:37', '1522227936899'),
(21, 'b4c724c6d5149c71d14a121340b0996c', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-28 14:36:08', '1522227968019'),
(22, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-28 14:36:27', '1522227987275'),
(23, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-28 14:36:47', '1522228007582'),
(24, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-28 14:37:03', '1522228023603'),
(25, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-28 14:37:23', '1522228043363'),
(26, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-28 14:37:41', '1522228061567'),
(27, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-28 14:38:01', '1522228081319'),
(28, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-28 14:39:36', '1522228176527'),
(29, 'b4c724c6d5149c71d14a121340b0996c', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-28 14:42:03', '1522228323558'),
(30, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-28 14:52:59', '1522228979591'),
(31, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-28 14:53:27', '1522229007520'),
(32, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-28 15:01:59', '1522229519235'),
(33, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-28 15:02:39', '1522229559287'),
(34, 'b4c724c6d5149c71d14a121340b0996c', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-28 15:04:56', '1522229696376'),
(35, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-28 19:39:31', '1522246171700'),
(36, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 13:31:17', '1522310477173'),
(37, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 13:31:46', '1522310506057'),
(38, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 13:48:02', '1522311481907'),
(39, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 13:50:02', '1522311602071'),
(40, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 13:50:12', '1522311612071'),
(41, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 14:55:35', '1522315535489'),
(42, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 14:56:21', '1522315580851'),
(43, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 14:56:38', '1522315598650'),
(44, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 14:57:01', '1522315621397'),
(45, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 15:02:40', '1522315959570'),
(46, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 15:13:01', '1522316580920'),
(47, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 15:37:32', '1522318052602'),
(48, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 15:37:42', '1522318062602'),
(49, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 15:39:47', '1522318187095'),
(50, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 15:41:32', '1522318291812'),
(51, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 15:43:15', '1522318394677'),
(52, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 16:45:42', '1522322142392'),
(53, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 16:46:20', '1522322180651'),
(54, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 16:47:12', '1522322232372'),
(55, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 16:59:51', '1522322990625'),
(56, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 17:00:20', '1522323020071'),
(57, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 17:00:32', '1522323032148'),
(58, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 17:01:36', '1522323096523'),
(59, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 17:01:46', '1522323106523'),
(60, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 17:02:14', '1522323134781'),
(61, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 17:03:00', '1522323179831'),
(62, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 17:03:54', '1522323234005'),
(63, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 17:04:17', '1522323257269'),
(64, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 17:54:05', '1522326244749'),
(65, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 17:54:44', '1522326284159'),
(66, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 17:55:22', '1522326322558'),
(67, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 17:57:40', '1522326460468'),
(68, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 17:58:03', '1522326483413'),
(69, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 18:02:10', '1522326730054'),
(70, 'b4c724c6d5149c71d14a121340b0996f', '::1', '18.52043030', '73.85674370', 'Saishwar Apartment, Chhatrapati Shivaji Maharaj Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', 9366, '', '', '', '', '2018-03-29 18:03:10', '1522326789831');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_user_activity`
--
ALTER TABLE `tbl_user_activity`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_user_activity`
--
ALTER TABLE `tbl_user_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
